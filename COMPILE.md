#HOWTO compile MTSatellite

To build MTSatellite a [Go](http://golang.org) compiler 1.3 or better is needed.

Currently this is only tested on Debian Wheezy, Debian Jessie,
Ubuntu Trusty Thar and Ubuntu Utopic Unicorn. Other flavors
of GNU/Linux should do, too. Mac OS X may work. Problems with MS Windows
are expected.

A quick and dirty way to produce the binaries of `mtdbconverter`,
`mtredisalize`, `mtseeder` and `mtwebmapper`:

    # Assuming you have a 64bit GNU/Linux system. For other systems take
    # the corresponding version from https://golang.org/dl/
    $ wget https://storage.googleapis.com/golang/go1.4.1.linux-amd64.tar.gz

    $ sha1sum go1.4.1.linux-amd64.tar.gz
    3e871200e13c0b059b14866d428910de0a4c51ed go1.4.1.linux-amd64.tar.gz

    $ tar xf go1.3.3.linux-amd64.tar.gz

    $ mkdir -p gopath/{pkg,bin,src}

    $ export GOROOT=`pwd`/go

    $ export GOPATH=`pwd`/gopath

    $ export PATH=$GOROOT/bin:$GOPATH/bin:$PATH

    # On Debian Wheezy you have to install the LevelDB dev from Backports.
    $ sudo apt-get install libleveldb-dev

    $ go get -u bitbucket.org/s_l_teichmann/mtsatellite/cmd/mtdbconverter

    $ go get -u bitbucket.org/s_l_teichmann/mtsatellite/cmd/mtredisalize

    $ go get -u bitbucket.org/s_l_teichmann/mtsatellite/cmd/mtseeder

    $ go get -u bitbucket.org/s_l_teichmann/mtsatellite/cmd/mtwebmapper

    $ ls $GOPATH/bin
    mtdbconverter  mtredisalize  mtseeder  mtwebmapper
