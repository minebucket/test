// Copyright 2014 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

import (
	"net"
	"strings"
)

func ipsFromHosts(hosts string) ([]net.IP, error) {

	ips := []net.IP{}

	if len(hosts) == 0 { // Empty list: allow all hosts.
		return ips, nil
	}

	for _, host := range strings.Split(hosts, ";") {
		hips, err := net.LookupIP(host)
		if err != nil {
			return nil, err
		}
		ips = append(ips, hips...)
	}

	return ips, nil
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
