// Copyright 2014 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

import "errors"

var ErrNotImplemented = errors.New("Not implemented")

type (
	Block struct {
		Key  []byte
		Data []byte
	}

	Session interface {
		Fetch(hash, key []byte) ([]byte, error)
		InTransaction() bool
		Store(hash, key, value []byte) (bool, error)
		AllKeys(hash []byte, done chan struct{}) (chan []byte, int, error)
		SpatialQuery(hash, first, second []byte, done chan struct{}) (chan Block, error)
		BeginTransaction() error
		CommitTransaction() error
		Close() error
	}

	Backend interface {
		NewSession() (Session, error)
		Shutdown() error
	}
)
