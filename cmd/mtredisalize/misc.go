// Copyright 2014 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

func order(a, b int64) (int64, int64) {
	if a < b {
		return a, b
	}
	return b, a
}
