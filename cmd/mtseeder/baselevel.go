// Copyright 2014 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

import (
	"log"
	"os"
	"path/filepath"
	"strconv"
	"sync"

	"bitbucket.org/s_l_teichmann/mtsatellite/common"
)

const (
	width          = 18
	height         = 18
	baseLevelDir   = "8"
	yOrderCapacity = 512
)

type blockPos struct {
	x, z int16
	j, i int
}

func createTiles(
	btc *common.BaseTileCreator,
	jobs chan blockPos,
	done *sync.WaitGroup) {

	defer done.Done()
	defer btc.Close()
	for job := range jobs {
		btc.CreateTile(job.x-1, job.z-1, job.i, job.j)
	}
}

func order(a, b int) (int, int) {
	if a < b {
		return a, b
	}
	return b, a
}

func createBaseLevel(
	address string,
	xMin, zMin, xMax, zMax int,
	transparent bool,
	colorsFile, outDir string,
	numWorkers int) (err error) {

	var colors *common.Colors

	if colors, err = common.ParseColors(colorsFile); err != nil {
		return
	}

	baseDir := filepath.Join(outDir, baseLevelDir)
	if err = os.MkdirAll(baseDir, os.ModePerm); err != nil {
		return
	}

	jobs := make(chan blockPos)
	var done sync.WaitGroup

	for i := 0; i < numWorkers; i++ {
		var client *common.RedisClient
		if client, err = common.NewRedisClient("tcp", address); err != nil {
			return
		}
		done.Add(1)
		btc := common.NewBaseTileCreator(client, colors, transparent, baseDir, false)
		go createTiles(btc, jobs, &done)
	}

	zMin, zMax = order(zMin, zMax)

	for x, i := int16(xMin), 0; x <= int16(xMax); x += 16 {
		xDir := filepath.Join(baseDir, strconv.Itoa(i))
		log.Printf("creating dir: %s", xDir)
		if err = os.MkdirAll(xDir, os.ModePerm); err != nil {
			log.Fatalf("Cannot create directory '%s': %s", xDir, err)
		}
		for z, j := int16(zMin), 0; z <= int16(zMax); z += 16 {
			jobs <- blockPos{x: x, z: z, i: i, j: j}
			j++
		}
		i++
	}
	close(jobs)

	done.Wait()

	return
}
