// Copyright 2014 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"log"
)

func main() {
	var (
		port          int
		host          string
		xMin, zMin    int
		xMax, zMax    int
		colorsFile    string
		outDir        string
		numWorkers    int
		skipBaseLevel bool
		skipPyramid   bool
		transparent   bool
	)

	flag.IntVar(&port, "port", 6379, "port to of mtredisalize server")
	flag.IntVar(&port, "p", 6379, "port to of mtredisalize server (shorthand)")
	flag.StringVar(&host, "host", "localhost", "host to mtredisalize server")
	flag.IntVar(&xMin, "xmin", -1933, "x min of the area to tile")
	flag.IntVar(&xMax, "xmax", 1932, "x max of the area to tile")
	flag.IntVar(&zMin, "zmin", -1933, "z min of the area to tile")
	flag.IntVar(&zMax, "zmax", 1932, "z max of the area to tile")
	flag.StringVar(&colorsFile, "colors", "colors.txt", "definition of colors")
	flag.StringVar(&outDir, "output-dir", "map", "directory with the resulting image tree")
	flag.StringVar(&outDir, "o", "map", "directory with the resulting image tree")
	flag.IntVar(&numWorkers, "workers", 1, "number of workers")
	flag.IntVar(&numWorkers, "w", 1, "number of workers (shorthand)")
	flag.BoolVar(&skipBaseLevel, "skip-base-level", false, "Do not generate base level tiles")
	flag.BoolVar(&skipBaseLevel, "sb", false, "Do not generate base level tiles (shorthand)")
	flag.BoolVar(&skipPyramid, "skip-pyramid", false, "Do not generate pyramid tiles")
	flag.BoolVar(&skipPyramid, "sp", false, "Do not generate pyramid tiles (shorthand)")
	flag.BoolVar(&transparent, "transparent", false, "Render transparent blocks.")
	flag.BoolVar(&transparent, "t", false, "Render transparent blocks (shorthand).")

	flag.Parse()

	var err error
	if !skipBaseLevel {
		address := fmt.Sprintf("%s:%d", host, port)
		if err = createBaseLevel(
			address,
			xMin, zMin, xMax, zMax,
			transparent,
			colorsFile,
			outDir,
			numWorkers); err != nil {
			log.Fatalf("Creating base level tiles failed: %s", err)
		}
	}
	if !skipPyramid {
		if err = createPyramid(outDir, numWorkers); err != nil {
			log.Fatalf("Creating pyramid tiles failed: %s", err)
		}
	}
}
