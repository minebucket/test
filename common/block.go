package common

import (
	"compress/zlib"
	"encoding/binary"
	"errors"
	"io"
	"io/ioutil"
	"log"
)

// Error returned if a Producer has run to its end.
var (
	ErrNoMoreBlocks           = errors.New("No more blocks.")
	ErrMapContentSizeMismatch = errors.New("Content size does not match.")
)

const (
	mapBlockSize = 16
	nodeCount    = mapBlockSize * mapBlockSize * mapBlockSize
)

type (
	// Block data from Minetest database.
	Block struct {
		Coord Coord
		Data  []byte
	}
	// BlockProducer is used to over a existing Minetest database
	// and return its content block by block.
	BlockProducer interface {
		// error is ErrNoMoreBlocks if it run out of blocks.
		Next(*Block) error
		// Closes the open database connections.
		Close() error
	}

	// BlockConsumer is used to store blocks in a new Minetest database.
	BlockConsumer interface {
		Consume(*Block) error
		// Closes the open database connections.
		Close() error
	}

	DecodedBlock struct {
		Version     byte
		MapContent  []byte
		AirId       int32
		IgnoreId    int32
		AirOnly     bool
		Transparent bool
		IndexMap    map[int32]int32
	}
)

// The content of the map and the meta data are compressed with zlib.
// Unfortunately the byte length of this two structures are not stored
// explicitly in the block data. To access the informations behind
// them (e.g. the node id mappings) we have to count the bytes consumed
// by the zlib reader and continue our extraction process behind this
// offset. posBuf implements such a counting reader source.
type posBuf struct {
	Data []byte
	Pos  int
}

func NewDecodedBlock(data []byte, colors *Colors) (db *DecodedBlock, err error) {
	version := data[0]

	contentWidth := int(data[2])
	paramsWidth := int(data[3])

	uncompressedLen := nodeCount * (contentWidth + paramsWidth)

	offset := 2
	if version >= 22 {
		offset = 4
	}

	buf := NewposBuf(data[offset:])
	var zr io.ReadCloser
	if zr, err = zlib.NewReader(buf); err != nil {
		return
	}

	var mapContent []byte
	mapContent, err = ioutil.ReadAll(zr)
	zr.Close()
	if err != nil {
		return
	}

	if uncompressedLen != len(mapContent) {
		err = ErrMapContentSizeMismatch
		return
	}

	offset += buf.Pos
	buf.Pos = 0
	buf.Data = data[offset:]

	if zr, err = zlib.NewReader(buf); err != nil {
		return
	}

	// Discard the meta data.
	_, err = io.Copy(ioutil.Discard, zr)
	zr.Close()
	if err != nil {
		return
	}

	offset += buf.Pos

	switch {
	case version <= 21:
		offset += 2
	case version == 23:
		offset++
	case version == 24:
		ver := data[offset]
		offset++
		if ver == 1 {
			num := int(binary.BigEndian.Uint16(data[offset:]))
			offset += 2 + 10*num
		}
	}

	offset++
	numStaticObjects := int(binary.BigEndian.Uint16(data[offset:]))
	offset += 2
	for i := 0; i < numStaticObjects; i++ {
		offset += 13
		dataSize := int(binary.BigEndian.Uint16(data[offset:]))
		offset += dataSize + 2
	}
	offset += 4

	airId, ignoreId := int32(-1), int32(-1)
	indexMap := make(map[int32]int32)
	var airOnly bool
	var transparent bool
	if version >= 22 {
		offset++
		numMappings := int(binary.BigEndian.Uint16(data[offset:]))
		offset += 2
		for i := 0; i < numMappings; i++ {
			nodeId := int32(binary.BigEndian.Uint16(data[offset:]))
			offset += 2
			nameLen := int(binary.BigEndian.Uint16(data[offset:]))
			offset += 2
			name := string(data[offset : offset+nameLen])
			offset += nameLen
			switch name {
			case "air":
				airId = nodeId
			case "ignore":
				ignoreId = nodeId
			default:
				if index, found := colors.NameIndex[name]; found {
					indexMap[nodeId] = index
					if colors.IsTransparent(index) {
						transparent = true
					}
				} else {
					log.Printf("Missing color entry for %s.", name)
				}
			}
		}
		airOnly = airId != -1 && len(indexMap) == 0
	}

	db = &DecodedBlock{
		Version:     version,
		MapContent:  mapContent,
		AirId:       airId,
		IgnoreId:    ignoreId,
		AirOnly:     airOnly,
		Transparent: transparent,
		IndexMap:    indexMap}

	return
}

func (db *DecodedBlock) Content(x, y, z int) (content int32, found bool) {
	pos := z<<8 + y<<4 + x

	switch {
	case db.Version >= 24:
		pos <<= 1
		content = int32(db.MapContent[pos])<<8 | int32(db.MapContent[pos+1])
	case db.Version >= 20:
		if db.MapContent[pos] <= 0x80 {
			content = int32(db.MapContent[pos])
		} else {
			content = int32(db.MapContent[pos])<<4 | int32(db.MapContent[pos+0x2000])>>4
		}
	default:
		return
	}
	if content != db.AirId && content != db.IgnoreId {
		content, found = db.IndexMap[content]
	}
	return
}

func NewposBuf(data []byte) *posBuf {
	return &posBuf{Data: data}
}

func (pb *posBuf) Read(p []byte) (int, error) {
	pl := len(p)
	ml := len(pb.Data)
	if pb.Pos >= ml {
		return 0, io.EOF
	}
	rest := ml - pb.Pos
	if pl > rest {
		copy(p, pb.Data[pb.Pos:])
		pb.Pos = ml
		return rest, io.EOF
	}
	copy(p, pb.Data[pb.Pos:pb.Pos+pl])
	pb.Pos += pl
	return pl, nil
}

func (pb *posBuf) ReadByte() (byte, error) {
	if pb.Pos >= len(pb.Data) {
		return 0, io.EOF
	}
	c := pb.Data[pb.Pos]
	pb.Pos++
	return c, nil
}
