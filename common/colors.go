// Copyright 2014 by Sascha L. Teichmann
// Use of this source code is governed by the MIT license
// that can be found in the LICENSE file.

package common

import (
	"bufio"
	"fmt"
	"image/color"
	"os"
	"sort"
	"strings"
)

type Colors struct {
	Colors         []color.RGBA
	NameIndex      map[string]int32
	NumTransparent int32
}

type namedColor struct {
	name  string
	color color.RGBA
}

type sortByAlpha []namedColor

func (colors sortByAlpha) Less(i, j int) bool {
	return colors[i].color.A < colors[j].color.A
}

func (colors sortByAlpha) Len() int {
	return len(colors)
}

func (colors sortByAlpha) Swap(i, j int) {
	colors[i], colors[j] = colors[j], colors[i]
}

func ParseColors(filename string) (colors *Colors, err error) {

	var file *os.File
	if file, err = os.Open(filename); err != nil {
		return
	}
	defer file.Close()

	cols := make([]namedColor, 0, 2200)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		c := color.RGBA{A: 0xff}
		var name string
		if n, _ := fmt.Sscanf(
			line, "%s %d %d %d %d", &name, &c.R, &c.G, &c.B, &c.A); n > 0 {
			cols = append(cols, namedColor{name: name, color: c})
		}
	}
	err = scanner.Err()

	// Sort transparent colors to front. Makes it easier to figure out
	// if an index corresponds to a transparent color (i < Transparent).
	sort.Sort(sortByAlpha(cols))

	cs := make([]color.RGBA, len(cols))
	nameIndex := make(map[string]int32, len(cols))

	numTransparent := int32(0)
	for i, nc := range cols {
		if nc.color.A < 0xff {
			numTransparent++
		}
		cs[i] = nc.color
		nameIndex[nc.name] = int32(i)
	}
	colors = &Colors{
		Colors:         cs,
		NameIndex:      nameIndex,
		NumTransparent: numTransparent}
	return
}

func (colors *Colors) IsTransparent(index int32) bool {
	return index < colors.NumTransparent
}

func min(a, b int32) int32 {
	if a < b {
		return a
	}
	return b
}

func BlendColor(c1, c2 color.RGBA, a float32) color.RGBA {
	b := float32(1) - a
	return color.RGBA{
		R: uint8(float32(c1.R)*a + float32(c2.R)*b),
		G: uint8(float32(c1.G)*a + float32(c2.G)*b),
		B: uint8(float32(c1.B)*a + float32(c2.B)*b),
		A: 0xff}
}

func (colors *Colors) BlendColors(span *Span, col color.RGBA, pos int32) color.RGBA {
	curr := span
	// Ignore colors below pos.
	for ; curr != nil && pos >= curr.To; curr = curr.Next {
	}
	if curr == nil {
		return col
	}
	const scale = float32(1) / float32(100)
	for ; curr != nil; curr = curr.Next {
		// At least 50% attenuation + 2% extra for each depth meter.
		factor := float32(min(100, 50+(curr.To-curr.From)*2)) * scale
		col = BlendColor(colors.Colors[curr.Value], col, factor)
	}
	return col
}
