# MTSatellite

MTSatellite is a "realtime" web mapping system for [Minetest](http://minetest.net) worlds.  
With this system you can play your world and you instantly have an online map of it which
can be shared on the web.

To get a glimpse what it does watch [Realtime Webmapping for Minetest worlds](http://youtu.be/iYEROGPj7RI)
on YouTube.  
A live map of an online world can be viewed [here](http://maps.mt.sha-bang.de/).

See [COMPILE](https://bitbucket.org/s_l_teichmann/mtsatellite/src/default/COMPILE.md) how to compile
MTSatellite. Essentially you need Go 1.3 (or higher) and a GNU/Linux system.

See [SETUP](https://bitbucket.org/s_l_teichmann/mtsatellite/src/default/SETUP.md) how to bring
MTSatellite to life.

To use it you have to configure your Minetest server to use the Redis backend data store.
Instead of a real Redis server you have to run **mtredisalize**. The data itself is not stored in a Redis server. It only uses the Redis protocol to communicate with the MT server. The data
is stored in an optimized form in a LevelDB or SQLite3 database. Changes made to the world are collected over a configurable amount
of time periodically. After a cycle of collection the web server **mtwebmapper** is triggered
to regenerate the map. To not fully regenerate the whole world map but only the differences
the map has to be pre-rendered with **mtseeder**.

* [mtredisalize](https://bitbucket.org/s_l_teichmann/mtsatellite/src/default/cmd/mtredisalize) is a server which has to be configured as a Redis backend to the
  Minetest server.

* [mtwebmapper](https://bitbucket.org/s_l_teichmann/mtsatellite/src/default/cmd/mtwebmapper/) Web server that serves [Leaflet](http://leafletjs.com) compatible tiles to a
  web browser. Running in the background this server updates the pre-computed map tiles.

* [mtdbconverter](https://bitbucket.org/s_l_teichmann/mtsatellite/src/default/cmd/mtdbconverter) is converter between Minetest databases. mtredisalize works
  best if you use a special interleaved LevelDB key schema. To bring your old database (LevelBD or SQLite3)
  into this format this tool does the job. It can also be used to convert the interleaved database
  back to a plain LevelDB or SQLite3 one.

* [mtseeder](https://bitbucket.org/s_l_teichmann/mtsatellite/src/default/cmd/mtseeder) pre-computes all tiles of the map of the world plus a set of pyramidal overview images.
  Even playing in a dynamic and fully changeable world most of the data is pretty constant over time. Therefore
  it makes sense to do only small updates on pre-calculated images instead of generating
  the map entirely on the fly.

This is Free Software under the terms of the MIT license.
See [LICENSE](LICENSE) file for details.  
(c) 2014 by Sascha L. Teichmann